mkdir -p kustomize/bootstrap
touch kustomize/bootstrap/kustomization.yaml

mkdir -p kustomize/base
touch kustomize/base/kustomization.yaml

mkdir -p kustomize/base/prometheus
touch kustomize/base/prometheus/kustomization.yaml

mkdir -p kustomize/base/extras
touch kustomize/base/extras/kustomization.yaml

mkdir -p kustomize/base/ingress
touch kustomize/base/ingress/kustomization.yaml

mkdir -p kustomize/overlays/
touch kustomize/overlays/kustomization.yaml

mkdir -p kustomize/overlays/prometheus
touch kustomize/overlays/prometheus/kustomization.yaml

mkdir -p kustomize/overlays/ingress
touch kustomize/overlays/ingress/kustomization.yaml

mkdir -p kustomize/overlays/generators
touch kustomize/overlays/generators/kustomization.yaml

mkdir -p kustomize/overlays/extras
touch kustomize/overlays/extras/kustomization.yaml