# prometheus-community-lcars



## Getting started
[blog.myezbrew.com](https://blog.myezbrew.com/prometheus-community-lcars/)

### Qucik Start
- Clone this repo
- Install Docaker
- Insatll k3d
- helm
- flux

To nuke the cluster:
```
> k3d cluster delete k3d-m1
INFO[0000] Deleting cluster 'k3d-m1'                    
INFO[0003] Deleting cluster network 'k3d-k3d-m1'        
INFO[0003] Deleting 1 attached volumes...               
INFO[0003] Removing cluster details from default kubeconfig... 
INFO[0003] Removing standalone kubeconfig file (if there is one)... 
INFO[0003] Successfully deleted cluster k3d-m1!         

```

To start the cluster: (override defaults or just keep hitting enter till it goes...)
```
> ./k3d-cluster
Cluster Name [k3d-m1]: 
Cluster Domain [localhost]: 
API Port [6443]: 
Servers (Masters) [1]: 
Agents (Workers) [1]: 
LoadBalancer HTTP Port [80]: 
LoadBalancer HTTPS Port [443]: 
... removed output
```

After 5 minutes or so these links will be avaliable:
- [prometheus](prometheus.localhost)
- [grafana](grafana.localhost)
- [alertmanager](alertmanager.localhost)
- [lens](promlens.localhost)

TODO:

